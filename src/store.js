import createLogger from "redux-logger";
import {createStore, applyMiddleware} from "redux";
import {routerMiddleware} from "react-router-redux";
import {browserHistory} from "react-router";
import restartGamesApp from "./reducer";

export default () => createStore(restartGamesApp, applyMiddleware(
    createLogger({
        collapsed: true,
        diff: true,
        predicate: (getState, action) => action.type !== 'GAMES_UPDATED'}),
    routerMiddleware(browserHistory))
)