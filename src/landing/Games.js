import React from "react";
import GamesIcon from "material-ui/svg-icons/hardware/videogame-asset";
import Title from "../ui/Title";

export default () => {
    return (
        <div style={{minHeight: "16rem"}}>
            <Title>Awesome Coop Games</Title>
            <GamesIcon style={{
                marginRight: "1rem",
                width: 130,
                height: 130,
                float: "right",
                color: "#d9b561"}}/>
            <div style={{width: "32rem", padding: "0.5rem 1rem"}}>
                <span>Restart Games is making it easy to play highly rated online coop games with your friends.
                    Currently available games are Factorio and Terraria.
                    Let us know which games you would like to play next.
                    </span>

            </div>
        </div>
    )
}
