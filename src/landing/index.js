import React from "react";
import First from "./First";
import Servers from "./Servers";
import Games from "./Games";
import Contact from "./Contact";

export default () => {
    return (
        <div style={{
            width: "48rem",
            margin: "0 auto 0"
        }}>
            <div style={{paddingRight: "1rem", paddingLeft: "1rem"}}>
                <First/>
                <Games/>
                <Servers/>
                <Contact/>
            </div>
        </div>

    )
}
