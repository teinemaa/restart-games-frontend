import React from "react";
import {connect} from "react-redux";
import RestartIcon from "material-ui/svg-icons/action/autorenew";
import GamesIcon from "material-ui/svg-icons/av/games";
import IconButton from "material-ui/IconButton";
import {Link} from "react-router";
import {push} from "react-router-redux";
import SignIn from "./SignIn";


const styles = {
    largeIcon: {
        width: 110,
        height: 110,
        color: "#d9b561"
    },
    large: {
        width: 110,
        height: 110,
        padding: 0
    },
};

const First = ({onSignIn}) => {
    return (
        <div>
            <div style={{height: "100vh",
                minHeight: "24rem",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexFlow: "column"}}>
                <div style={{
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "row",
                    fontSize: "3.8rem"}}>
                    Restart
                    <div style={{
                        marginTop: "1rem",
                        marginLeft: "0.6rem",
                        marginRight: "0.9rem",
                        marginBottom: "1rem"
                    }}>
                    <IconButton
                        containerElement={<Link to="restart"/>}
                        iconStyle={styles.largeIcon}
                        style={styles.large}>
                        <RestartIcon/>
                    </IconButton>
                    <IconButton
                        containerElement={<Link to="games"/>}
                        iconStyle={styles.largeIcon}
                        style={styles.large}>
                        <GamesIcon />
                    </IconButton>
                    </div>
                    Games
                </div>
                <div style={{fontSize: "2.1rem", lineHeight: "2.1rem", margin: "1rem"}}>Free game servers for awesome coop games</div>
                <div style={{marginBottom: "0.5rem", marginTop: "2rem"}}><SignIn onSignIn={onSignIn}/></div>
            </div>
        </div>
    )
}

export default connect(
    null,
    (dispatch) => ({
        onSignIn: (email) => {
            dispatch({ type: "SIGN_IN", email: email })
            dispatch(push("/restart"))
        }
    })
)(First)

