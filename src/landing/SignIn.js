/*global gapi*/
import React from "react";

export default class Greeting extends React.Component {
    componentDidMount(){
        gapi.load('auth2', () => {
            gapi.auth2.init({client_id: "128565711908-cia1jrie4knod0rcj6frfgdc40q5cr8l.apps.googleusercontent.com"})
                .then((auth2) => {
                    let alreadySignedIn = auth2.isSignedIn.get();
                    let firstRun = true
                    gapi.signin2.render('g-signin2', {
                        'width': 230,
                        'height': 50,
                        'longtitle': true,
                        'theme': 'dark',
                        'onsuccess': (googleUser) => {
                            if (alreadySignedIn && firstRun) {
                                firstRun = false
                            } else {
                                this.props.onSignIn(googleUser.getBasicProfile().getEmail())
                            }
                        }
                    });
                });
        });
    }
    render() {
        return (
            <div id="g-signin2"/>
        );
    }
}