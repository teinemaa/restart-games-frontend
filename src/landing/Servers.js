import React from "react";
import ServersIcon from "material-ui/svg-icons/hardware/memory";
import Title from "../ui/Title";

export default () => {
    return (
        <div style={{minHeight: "16rem"}}>
            <Title>Free game servers</Title>
            <ServersIcon style={{
                marginRight: "1rem",
                width: 130,
                height: 130,
                float: "right",
                color: "#d9b561"}}/>
            <div style={{width: "32rem", padding: "0.5rem 1rem"}}>
                <span>
                    Game servers are running on demand - only when you and your friends are playing.
                    Currently servers are limited to 24h per week to keep the service free of charge.
                    Your game gets 1 dedicated Intel Xeon CPU core, 1GB of memory and SSD storage.
                    </span>

            </div>
        </div>
    )
}
