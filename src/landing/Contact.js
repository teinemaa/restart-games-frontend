import React from "react";
import ServersIcon from "material-ui/svg-icons/communication/email";
import Title from "../ui/Title";

export default () => {
    return (
            <div style={{minHeight: "16rem"}}>
                <Title>Contact Us</Title>
                <ServersIcon style={{
                    marginRight: "1rem",
                    width: 130,
                    height: 130,
                    float: "right",
                    color: "#d9b561"}}/>
                <div style={{width: "32rem", padding: "0.5rem 1rem"}}>
                    <span>Restart Games is still in very early stage and all sorts of feedback is welcome at teinemaa@gmail.com
                        </span>

                </div>
            </div>
    )
}
