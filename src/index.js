import ReactDOM from "react-dom";
import injectTapEventPlugin from "react-tap-event-plugin";
import createView from "./view";
import createStore from "./store";
import syncStore from "./syncStore";
import "./index.css";

let store = createStore()
window.store = store

syncStore(store)

injectTapEventPlugin()

ReactDOM.render(
    createView(store),
    document.getElementById('root')
)
