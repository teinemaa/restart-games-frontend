class Rest {
    addUser(headers) {
        let user = window.store.getState().user
        return user ? Object.assign({user: user}, headers) : headers
    }
    get(path) {
        return fetch(`/api${path}`, {
                method: 'GET',
                headers: this.addUser({})
            })
            .then((response) => {
                    if (response.ok) return response;
                    else throw Error(response.statusText) },
                () => {})
            .then(
                (response) => response.json(),
                () => {})
    }
    post(path, body) {
        return fetch(`/api${path}`, {
            method: 'POST',
            headers: this.addUser({
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(body)
        })
    }
    patch(path, id, body) {
        return fetch(`/api${path}/${id}`, {
            method: 'PATCH',
            headers: this.addUser({
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify(body)
        })
    }
    delete(path, id) {
        return fetch(`/api${path}/${id}`, {
            method: 'DELETE',
            headers: this.addUser({})
        })
    }
}

export default new Rest()