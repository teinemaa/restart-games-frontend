import React from "react";
import Divider from "material-ui/Divider";

export default ({children}) => {
    return (
        <div style={{
            padding: "1rem"
        }}>
            <div style={{
                fontWeight: "400",
                marginBottom: "1rem",
                fontSize: "1.8rem"
            }}>
                {children}
            </div>
            <Divider/>
        </div>
    )
}

