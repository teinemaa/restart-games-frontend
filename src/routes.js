import React from "react";
import {Route, IndexRoute} from "react-router";
import App from "./App";
import LoggedIn from "./LoggedIn";
import Restart from "./restart/index";
import Games from "./games/index";
import Support from "./support/index";
import Account from "./account/index";
import Landing from "./landing/index";

const requireAuth = (store, nextState, replace) => {
    if (!store.getState().user) {
        replace({
            pathname: '/',
            state: { nextPathname: nextState.location.pathname }
        })
    }
}

export default (store) => (
    <Route component={App} path="/">
        <IndexRoute component={Landing}/>
        <Route component={LoggedIn} onEnter={requireAuth.bind(null, store)} >
            <Route path="restart" component={Restart}/>
            <Route path="games" component={Games}/>
            <Route path="support" component={Support}/>
            <Route path="account" component={Account}/>
        </Route>
    </Route>
)