import rest from "./common/rest";

let update = (store) => rest.get("/games")
    .then((games) => store.dispatch({type: 'GAMES_UPDATED', games}))
    .then(() => setTimeout(update, 500, store));

export default (store) => update(store)