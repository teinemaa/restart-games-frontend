import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import games from "./games/reducer";
import user from "./reducers/user";
import {gameTypes} from "./restart/reducer";

const restartGamesApp = combineReducers({
    games: games,
    restart: combineReducers({gameTypes}),
    user: user,
    routing: routerReducer
})

export default restartGamesApp
