export const NEW_GAME = 'NEW_GAME'
export const newGame = (game) => ({ type: NEW_GAME, newGame: game })
