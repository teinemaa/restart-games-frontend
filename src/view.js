import React from "react";
import {Provider} from "react-redux";
import {browserHistory, Router} from "react-router";
import {syncHistoryWithStore} from "react-router-redux";
import routes from "./routes";

export default (store) => (
    <Provider store={store}>
        <Router history={syncHistoryWithStore(browserHistory, store)}>
            {routes(store)}
        </Router>
    </Provider>
)