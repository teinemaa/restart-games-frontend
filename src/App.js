import React from "react";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: "#ffffff",
        borderColor: "#4285f4",
        accent1Color: "#4285f4",
        alternateTextColor: "#555555",
    }
});

export default ({children}) => {
    return (
        <MuiThemeProvider muiTheme={muiTheme}>
            {children}
        </MuiThemeProvider>
    )
}
