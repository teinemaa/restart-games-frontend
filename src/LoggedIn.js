import React from "react";
import Header from "./components/Header";

export default ({children}) => (
    <div>
        <Header />
        <div style={{
            width: "48rem",
            margin: "0 auto 0",
            padding: "1rem"
        }}>
            <div style={{paddingTop: "0.7rem"}}>
        {children}
            </div>
        </div>
    </div>
)
