import React from "react";
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow} from "material-ui/Table";
import {connect} from "react-redux";
import rest from "../common/rest";
import Title from "../ui/Title";
import GameRow from "./GameRow";

const Games = ({games, onSave, onResume, onDelete}) => {
    return (
        <div>
            <Title>Games</Title>
            <div style={{paddingLeft: "1rem", paddingRight: "1rem"}}>
            <Table style={{ tableLayout: "auto" }} fixedHeader={false} selectable={false}>
            <TableHeader adjustForCheckbox={false}
                         displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn style={{textAlign: "center"}}>Game</TableHeaderColumn>
                    <TableHeaderColumn style={{textAlign: "center"}}>Nr</TableHeaderColumn>
                    <TableHeaderColumn style={{textAlign: "center"}}>Status</TableHeaderColumn>
                    <TableHeaderColumn style={{textAlign: "center"}}>IP Address</TableHeaderColumn>
                    <TableHeaderColumn style={{textAlign: "center"}}>Shutdown in</TableHeaderColumn>
                    <TableHeaderColumn style={{textAlign: "center"}}>Actions</TableHeaderColumn>
                </TableRow>
            </TableHeader>
                <TableBody displayRowCheckbox={false}>
                    {games.map(game => <GameRow
                        key={game.id}
                        game={game}
                        onSave={() => onSave(game.id)}
                        onResume={() => onResume(game.id)}
                        onDelete={() => onDelete(game.id)}
                    />)}
                </TableBody>
            </Table>
            </div>
        </div>
    )
}

export default connect(
    (state) => ({
        games: state.games
    }),
    (dispatch) => ({
        onSave: gameId => {
            dispatch({ type: "GAME_SAVE", id: gameId })
            rest.patch("/games", gameId, { id: gameId, running: false })
        },
        onResume: gameId => {

            dispatch({ type: "GAME_RESUME", id: gameId })
            rest.patch("/games", gameId, { id: gameId, running: true })
        },
        onDelete: gameId => {
            dispatch({ type: "GAME_DELETE", id: gameId })
            rest.delete("/games", gameId)
        }
    })
)(Games)
