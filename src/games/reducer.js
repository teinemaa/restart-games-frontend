export default (state = [], action) => {
    switch (action.type) {
        case "GAME_DELETE":
        case "GAME_SAVE":
        case "GAME_RESUME":
            return state.filter((game) => game.id !== action.id)
        case "GAMES_UPDATED":
            return action.games
        default:
            return state
    }
}

