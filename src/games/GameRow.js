import React from "react";
import {TableRow, TableRowColumn} from "material-ui/Table";
import IconButton from "material-ui/IconButton";
import ErrorIcon from "material-ui/svg-icons/alert/error";
import BusyIcon from "material-ui/svg-icons/action/hourglass-empty";
import RunningIcon from "material-ui/svg-icons/av/play-arrow";
import SavedIcon from "material-ui/svg-icons/av/stop";
import SaveIcon from "material-ui/svg-icons/content/save";
import DeleteIcon from "material-ui/svg-icons/action/delete";
import ResumeIcon from "material-ui/svg-icons/av/play-circle-outline";

export default ({game, onSave, onResume, onDelete}) => {
    let formatSeconds = (seconds) => {
        let time = new Date(null)
        time.setSeconds(seconds)
        return time.toISOString().substr(11, 8)
    }
    let statusIcon = {color: "#4285f4", width: "1.4rem", height: "1.4rem", padding: 0, marginTop: "-1rem", marginBottom: "-0.4rem"}
    let actionButton = {width: "2.4rem", height: "2.4rem", padding: 0, marginTop: "0", marginBottom: "-0.4rem"}
    let actionIcon = {color: "#d9b561", width: "1.4rem", height: "1.4rem"}
    return (
        <TableRow>
            <TableRowColumn style={{textAlign: "center"}}>{game.typeDescription}</TableRowColumn>
            <TableRowColumn style={{textAlign: "center"}}>{game.name}</TableRowColumn>
            {game.error && <TableRowColumn style={{textAlign: "center"}}><ErrorIcon style={statusIcon}/></TableRowColumn>}
            {game.busy && <TableRowColumn style={{textAlign: "center"}}><BusyIcon style={statusIcon}/></TableRowColumn>}
            {game.running && <TableRowColumn style={{textAlign: "center"}}><RunningIcon style={statusIcon}/></TableRowColumn>}
            {game.saved && <TableRowColumn style={{textAlign: "center"}}><SavedIcon style={statusIcon}/></TableRowColumn>}
            <TableRowColumn style={{textAlign: "center"}}>{game.ipAddress}</TableRowColumn>
            <TableRowColumn style={{textAlign: "center"}}>{formatSeconds(game.shutdownInSeconds)}</TableRowColumn>
            <TableRowColumn style={{textAlign: "center"}}>
                {game.running && <IconButton iconStyle={actionIcon} style={actionButton}><SaveIcon onClick={onSave} /></IconButton>}
                {game.saved && <IconButton iconStyle={actionIcon} style={actionButton}><ResumeIcon onClick={onResume} /></IconButton>}
                {game.ready && <IconButton iconStyle={actionIcon} style={actionButton}><DeleteIcon onClick={() => confirm("Do you want to delete your "+game.typeDescription+" server?") && onDelete()}/></IconButton>}
            </TableRowColumn>
        </TableRow>
    )
}

