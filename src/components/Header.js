/*global gapi*/
import React from "react";
import RestartIcon from "material-ui/svg-icons/action/autorenew";
import GamesIcon from "material-ui/svg-icons/av/games";
import SupportIcon from "material-ui/svg-icons/action/feedback";
import AccountIcon from "material-ui/svg-icons/action/exit-to-app";
import {Tabs, Tab} from "material-ui/Tabs";
import Paper from "material-ui/Paper";
import {Link} from "react-router";
import {connect} from "react-redux";
import {push} from "react-router-redux";

const Header = ({onSignOut}) => {
    let buttonSize = "3.2rem"
    return (
        <div>
            <Paper>
                <Tabs style={{width: "48rem", margin: "0 auto"}}>
                    <Tab icon={<div><RestartIcon style={{width:buttonSize, height:buttonSize, color: "#d9b561"}}/></div>} containerElement={<Link to="restart"/>} buttonStyle={{height: buttonSize, paddingTop: "1rem", paddingBottom: "1rem"}} />
                    <Tab icon={<div><GamesIcon style={{width:buttonSize, height:buttonSize, color: "#d9b561"}}/></div>} containerElement={<Link to="games"/>} buttonStyle={{height: buttonSize, paddingTop: "1rem", paddingBottom: "1rem"}} />
                    <Tab icon={<div><SupportIcon style={{width:buttonSize, height:buttonSize, color: "#d9b561"}}/></div>} containerElement={<Link to="support"/>} buttonStyle={{height: buttonSize, paddingTop: "1rem", paddingBottom: "1rem"}} />
                    <Tab icon={<div><AccountIcon style={{width:buttonSize, height:buttonSize, color: "#d9b561"}}/></div>} onClick={onSignOut} buttonStyle={{height: buttonSize, paddingTop: "1rem", paddingBottom: "1rem"}} />
                </Tabs>
            </Paper>
        </div>
    )
}

export default connect(
    null,
    (dispatch) => ({
        onSignOut: () => {
            dispatch({type: 'SIGN_OUT'})
            dispatch(push('/'))
        }
    })
)(Header)
