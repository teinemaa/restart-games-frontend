export default (state = sessionStorage.getItem("user"), action) => {
    var newState
    switch (action.type) {
        case "SIGN_IN":
             newState = action.email
            break
        case "SIGN_OUT":
            newState = null
            break
        default:
            newState = state
            break
    }
    newState ? sessionStorage.setItem("user", newState) : sessionStorage.removeItem("user")
    return newState
}