import React from "react";
import {Card, CardMedia, CardTitle} from "material-ui/Card";
import FlatButton from "material-ui/FlatButton";
import Divider from "material-ui/Divider";

export default ({ title, id, onClick }) => {
    return (
        <FlatButton
            style={{height: undefined}}
            onClick={() => onClick(id)}
            children={<Card style={{
                width: "10rem",
                margin: "1rem",
                borderStyle: "solid",
                borderWidth: "1px",
                borderColor: "#4285f4",
            }}>
                <CardTitle title={title}/>
                <Divider/>
                <CardMedia>
                    <img src={`images/${id}.png`} alt={title} />
                </CardMedia>
            </Card>}
        />
    )
}