import React from "react";
import {connect} from "react-redux";
import {push} from "react-router-redux";
import rest from "../common/rest";
import Title from "../ui/Title";
import GameButton from "./GameButton";
import {newGame} from "../actions";

const RestartPage = ({ gameTypes, onSubmit }) => {
    return (
        <div>
            <Title>New Game</Title>
            { gameTypes.map((gameType) => <GameButton key={gameType.id} title={gameType.title} id={gameType.id} onClick={(id) => onSubmit({type: id})} />) }
        </div>
    )
}

export default connect(
    (state) => ({
        gameTypes: state.restart.gameTypes
    }),
    (dispatch) => ({
        onSubmit: game => {
            dispatch(newGame(game))
            dispatch(push('/games'))
            rest.post("/games", game)
        }
    })
)(RestartPage)
